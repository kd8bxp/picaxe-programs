symbol clock = 0
symbol sdata = 2
symbol latch = 1
symbol delaytime = 100
symbol counter = b7
symbol mask = w4			; bit masking variable
symbol var_in = w5			; data variable used durig shiftin
symbol var_out = w6			; data variable used during shiftout
symbol bits = 8				; number of bits
symbol MSBvalue = 128

high latch
main:
do
mask = %10000000
gosub out595
mask = %01000000
gosub out595
mask = %00100000
gosub out595
mask = %00010000
gosub out595
mask = %00001000
gosub out595
mask = %00000100
gosub out595
mask = %00000010
gosub out595
mask = %00000001
gosub out595
mask = %00000010
gosub out595
mask = %00000100
gosub out595
mask = %00001000
gosub out595
mask = %00010000
gosub out595
mask = %00100000
gosub out595
mask = %01000000
gosub out595
loop

out595:
for counter = 1 to bits 	� number of bits
	mask = var_out & MSBValue 	� mask MSB
	high sdata 			� data high
	if mask = 0 then skipMSB
	low sdata 			� data low
skipMSB: pulsout clock,1 		� pulse clock for 10us
	var_out = var_out * 2 		� shift variable left for MSB
	next counter
	return
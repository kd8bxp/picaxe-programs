symbol sdata = 0
symbol rclk = 1
symbol sclk = 2
symbol dig1 = %10000000
symbol dig2 = %01000000
symbol dig3 = %00100000
symbol dig4 = %00010000

main:
setfreq m32
low sclk
low rclk
do
b3 = dig1
gosub writed
b3 = dig2
gosub writed
b3 = dig3
gosub writed
b3 = dig4
gosub writed
loop

writed:

for b2 = 0 to 7
if bit24 = 0 then : low sdata : else : high sdata : endif ;
let b3 = b3 /2
pulsout sclk, 1
next b2

return
